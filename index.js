var fs = require('fs');
var express = require('express');
var app = express();
var orgs_proj_info = JSON.parse(fs.readFileSync('project_catcher/orgs_proj_info.json', 'utf8'));

// language icon on project thumbnail
var lang_ico = {
	"JavaScript": {
		"class": "fab fa-js-square",
		"value": ""
	},
	"HTML": {
		"class": "fab fab fa-html5",
		"value": ""
	},
	"Java": {
		"class": "fab fa-java",
		"value": ""
	},
	"PHP": {
		"class": "fab fa-php",
		"value": ""
	},
	"CSS": {
		"class": "fab fa-css3-alt",
		"value": ""
	},
	"Python": {
		"class": "fab fa-python",
		"value": ""
	},
	"Shell": {
		"class": "fas fa-terminal",
		"value": ""
	},
	"C": {
		"class": "c",
		"value": "c"
	},
	"C++": {
		"class": "cpp",
		"value": "c++"
	},
}


app.set('view engine', 'ejs');

app.use(express.static('public'));

// index page
app.get('/', function(req, res) {
    res.render('home', {
		orgs_proj_info: orgs_proj_info,
		lang_ico: lang_ico
	});
});

app.listen(8080);
