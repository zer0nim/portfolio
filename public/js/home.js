$(document).ready(function()
{
	$('.proj_ico').click(function() {
		$('body').toggleClass('show_home show_projects')
		var proj_ico  = $('h3');
		proj_ico.text($(proj_ico[0]).text() == 'Projects' ? 'Close' : 'Projects');

	});

	grid_update();
	$(window).resize(grid_update);


	$(".wlc").each(function(index)
	{
		var total_len = $(this)[0].getTotalLength();
		$(this).css('stroke-dasharray', total_len);
		$(this).css('stroke-dashoffset', total_len);
		$(this).css('-webkit-animation', 'dash 1.5s .5s linear forwards');
		$(this).css('-moz-animation', 'dash 1.5s .5s linear forwards');
		$(this).css('-o-animation', 'dash 1.5s .5s linear forwards');
		$(this).css('animation', 'dash 1.5s .5s linear forwards');
		$(this).css('animation-timing-function', 'ease-in-out');
	});


});

function grid_update()
{
	var $proj = $('.proj');
	var nb = $proj.length;
	var wt = $('article').outerWidth();

	var minw = parseInt($proj.css('min-width'));
	var extra_width = parseFloat($proj.css('margin-left')) + parseFloat($proj.css('margin-right')) + parseFloat($proj.css('padding-left')) + parseFloat($proj.css('padding-right'));
	var nb_item_line = parseInt(wt / (minw + extra_width));

	$('.push_content').remove();
	nb_push = (nb % nb_item_line !== 0) ? nb_item_line - nb % nb_item_line : 0;
	for (var i = 0 ; i < nb_push ; i++)
	{
		$('article').append($('<div class="push_content"></div>'));
	}
}
